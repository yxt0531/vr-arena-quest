extends Control

var player

func _ready():
	# var init
	player = find_parent("Player")
	# var init end

func _process(_delta):
	if player.current_ammo != null:
		$LabelCurrentAmmo.text = "Ammo: " + str(player.current_ammo)

	if player.current_health != null:
		$LabelCurrentHP.text = "HP: " + str(player.current_health)
