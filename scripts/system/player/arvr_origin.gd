extends ARVROrigin

var arvr_interface = null

var ovr_init_config = null

var ovr_display_refresh_rate = null
var ovr_guardian_system = null
var ovr_performance = null
var ovr_tracking_transform = null
var ovr_utilities = null
var ovr_vr_api_proxy = null

var ovrVrApiTypes = load("res://addons/godot_ovrmobile/OvrVrApiTypes.gd").new()

var _performed_runtime_config = false

func _ready():
	_initialize_ovr_mobile_arvr_interface()

func _process(_delta):
	_check_and_perform_runtime_config()

func _initialize_ovr_mobile_arvr_interface():
	arvr_interface = ARVRServer.find_interface("OVRMobile")
	
	if !arvr_interface:
		print("couldn't find OVRMobile interface")
	else:
		# the init config needs to be done before arvr_interface.initialize()
		ovr_init_config = load("res://addons/godot_ovrmobile/OvrInitConfig.gdns")
		if (ovr_init_config):
			ovr_init_config = ovr_init_config.new()
			ovr_init_config.set_render_target_size_multiplier(0.75) # setting to 1 here is the default

		if arvr_interface.initialize():
			get_viewport().arvr = true
			Engine.target_fps = 72 # oculus quest refresh rate

			# load the .gdns classes.
			ovr_display_refresh_rate = load("res://addons/godot_ovrmobile/OvrDisplayRefreshRate.gdns")
			ovr_guardian_system = load("res://addons/godot_ovrmobile/OvrGuardianSystem.gdns")
			ovr_performance = load("res://addons/godot_ovrmobile/OvrPerformance.gdns")
			ovr_tracking_transform = load("res://addons/godot_ovrmobile/OvrTrackingTransform.gdns")
			ovr_utilities = load("res://addons/godot_ovrmobile/OvrUtilities.gdns")
			ovr_vr_api_proxy = load("res://addons/godot_ovrmobile/OvrVrApiProxy.gdns")
			
			# and now instance the .gdns classes for use if load was successfull
			if (ovr_display_refresh_rate):
				ovr_display_refresh_rate = ovr_display_refresh_rate.new()
			if (ovr_guardian_system):
				ovr_guardian_system = ovr_guardian_system.new()
			if (ovr_performance):
				ovr_performance = ovr_performance.new()
			if (ovr_tracking_transform):
				ovr_tracking_transform = ovr_tracking_transform.new()
			if (ovr_utilities):
				ovr_utilities = ovr_utilities.new()
			if (ovr_vr_api_proxy):
				ovr_vr_api_proxy = ovr_vr_api_proxy.new()

		else:
			print("failed to enable OVRMobile")

func _notification(what):
	if (what == NOTIFICATION_APP_PAUSED):
		pass
	if (what == NOTIFICATION_APP_RESUMED):
		_performed_runtime_config = false # redo runtime config

func _check_and_perform_runtime_config():
	if _performed_runtime_config: return

	if (ovr_performance):
		# these are some examples of using the ovr .gdns APIs
		# ovr_performance.set_clock_levels(1, 1)
		ovr_performance.set_extra_latency_mode(ovrVrApiTypes.OvrExtraLatencyMode.VRAPI_EXTRA_LATENCY_MODE_ON)
		# ovr_performance.set_foveation_level(2)  # 0 == off; 4 == highest
		# ovr_performance.set_enable_dynamic_foveation(true)

	_performed_runtime_config = true
