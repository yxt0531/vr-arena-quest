extends ImmediateGeometry

# internal use only
var toggle_released
var max_spray_angle
var spray_rays

func _ready():
	# var init
	toggle_released = true
	max_spray_angle = null
	spray_rays = null
	# var init end

	if SettingsWeapon.laser_on_by_default:
		visible = true

	_apply_color(Helper.available_colors[SettingsWeapon.laser_color])

func update_color():
	_apply_color(Helper.available_colors[SettingsWeapon.laser_color])

func _apply_color(color):
	# apply laser pointer color from global weapon setting
	var laser_material = SpatialMaterial.new()
	laser_material.flags_unshaded = true
	laser_material.flags_use_point_size = true
	laser_material.albedo_color = color
	material_override = laser_material

func _physics_process(_delta):
	if $RayCast.get_collider() != null:
		clear()
		begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		add_vertex(Vector3(0, 0, 0))
		add_vertex(to_local($RayCast.get_collision_point()))
		end()
	else:
		clear()
		begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		add_vertex(Vector3(0, 0, 0))
		add_vertex(Vector3(0, 0, -1000))
		end()
		
	if spray_rays != null:
		for spray_ray in spray_rays:
			randomize()
			spray_ray.rotation_degrees.x = randi() % (max_spray_angle + 1)
			randomize()
			spray_ray.rotation_degrees.y = randi() % (max_spray_angle + 1)
			randomize()
			spray_ray.rotation_degrees.z = randi() % (max_spray_angle + 1)

	
func toggle_laser_pressed():
	if toggle_released:
		visible = not visible
		toggle_released = false
	
func toggle_laser_just_released():
	toggle_released = true

func is_colliding():
	return $RayCast.is_colliding()
	
func get_collision_point():
	return $RayCast.get_collision_point()

func enable_multishot_per_round(rounds, msa, round_multiplier = 2):
	rounds *= round_multiplier
	
	print("enabling multishot with " + str(rounds) + " max spray angle " + str(msa))
	
	max_spray_angle = msa
	spray_rays = []
	while rounds > 0:
		var new_spary_ray = $RayCast.duplicate()
		add_child(new_spary_ray)
		spray_rays.append(new_spary_ray)
		rounds -= 1

func get_collision_points():
	var collision_points = []
	
	if spray_rays != null:
		for spray_ray in spray_rays:
			if spray_ray.is_colliding():
				collision_points.append(spray_ray.get_collision_point())
				
	return collision_points

func get_collision_object():
	return $RayCast.get_collider()
	
func get_collision_objects():
	var collision_objects = []
	
	if spray_rays != null:
		for spray_ray in spray_rays:
			if spray_ray.is_colliding():
				collision_objects.append(spray_ray.get_collider())
				
	return collision_objects
