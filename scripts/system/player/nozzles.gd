extends Spatial

enum MODE {
	STANDBY = 0,
	ASCENDING = 1,
	DESCENDING = 2
}

var prev_mode
var current_mode

func _ready():
	# var init
	prev_mode = MODE.STANDBY
	current_mode = MODE.STANDBY
	# var init end

func _process(_delta):
	# only execute on mode change
	return # deprecated

#	if prev_mode != current_mode:
#		match current_mode:
#			MODE.STANDBY:
#				for cpu_particle in get_children():
#					cpu_particle.emitting = true
#					cpu_particle.amount = 4
#					cpu_particle.initial_velocity = 1
#			MODE.ASCENDING:
#				for cpu_particle in get_children():
#					cpu_particle.emitting = true
#					cpu_particle.amount = 6
#					cpu_particle.initial_velocity = 2
#			MODE.DESCENDING:
#				for cpu_particle in get_children():
#					cpu_particle.emitting = false
#					cpu_particle.amount = 2
#					cpu_particle.initial_velocity = 0.5
#		prev_mode = current_mode
