extends ARVRController

const RUMBLE_RECOVER_SPEED = 10

var player

func _ready():
	# var init
	player = find_parent("Player")
	# var init end

func _process(delta):
	if rumble > 0:
		rumble -= delta * RUMBLE_RECOVER_SPEED

func _physics_process(delta):
	if player.arvr_origin.arvr_interface != null:
		if get_joystick_axis(Helper.CONTROLLER_AXIS.GRIP_TRIGGER) > SettingsWeapon.grip_threshold:
			player.swap_weapon(1)
		
		if get_child_count() > 0:
			if get_joystick_axis(Helper.CONTROLLER_AXIS.INDEX_TRIGGER) > SettingsWeapon.trigger_threshold:
				get_child(0).fire_pressed(delta)
			else:
				get_child(0).fire_just_released()
		
func _on_LeftTouchController_button_pressed(button):
	if get_child_count() > 0:
		if get_child(0) and button == Helper.CONTROLLER_BUTTON.THUMBSTICK:
			get_child(0).toggle_laser_pressed()

func _on_LeftTouchController_button_release(button):
	if get_child_count() > 0:
		if get_child(0) and button == Helper.CONTROLLER_BUTTON.THUMBSTICK:
			get_child(0).toggle_laser_just_released()
