extends Area

var cover_plane_material

func _ready():
	cover_plane_material = $CoverPlane.get_surface_material(0)

func _physics_process(delta):
	if get_overlapping_bodies().size() > 0:
		if cover_plane_material.albedo_color.a < 1:
			cover_plane_material.albedo_color.a += delta * 4
	else:
		if cover_plane_material.albedo_color.a > 0:
			cover_plane_material.albedo_color.a -= delta * 2
		else:
			cover_plane_material.albedo_color.a = 0
