extends Control

const RETURN_TO_LOBBY_SPEED = 1
const LABEL_INFO_TIMEOUT = 2

var player
var is_returning_to_lobby

var label_info_timer

func _ready():
	# var init
	player = find_parent("Player")
	is_returning_to_lobby = false
	
	label_info_timer = 0
	
	# var init end
	
func _process(delta):
	if $LabelInfo.text != "":
		label_info_timer += delta
		if label_info_timer >= LABEL_INFO_TIMEOUT:
			$LabelInfo.text = ""
	else:
		label_info_timer = 0
		
	if find_parent("MainMenu") == null:
		if NetworkPeerController.is_connected_ok:
			if not is_returning_to_lobby and player.get_node("ARVROrigin/LeftTouchController").is_button_pressed(Helper.CONTROLLER_BUTTON.YB):
				$LabelReturnToLobby.visible = true
				$ReturnToLobbyProgressBar.visible = true
				if $ReturnToLobbyProgressBar.rect_scale.x < 1:
					$ReturnToLobbyProgressBar.rect_scale.x += delta * RETURN_TO_LOBBY_SPEED
				else:
					is_returning_to_lobby = true
					NetworkPeerController.disconnect_fron_server()
			else:
				$LabelReturnToLobby.visible = false
				$ReturnToLobbyProgressBar.visible = false
				$ReturnToLobbyProgressBar.rect_scale.x = 0
		else:
			$LabelReturnToLobby.visible = false
			$ReturnToLobbyProgressBar.visible = false
			$ReturnToLobbyProgressBar.rect_scale.x = 0
				
			$LabelInfo.text = "Server Disconnected,\nReturning to Lobby..."
			$LabelInfo.visible = true
			
