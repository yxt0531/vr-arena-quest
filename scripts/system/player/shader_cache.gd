extends Spatial

const REQUIRED_FRAMES = 2

var frame_counter

func _ready():
	frame_counter = 0

func _process(delta):
	if frame_counter < REQUIRED_FRAMES:
		frame_counter += 1
	elif frame_counter == REQUIRED_FRAMES:
		visible = false
		frame_counter += 1
	else:
		return
