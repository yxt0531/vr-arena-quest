extends Area

func _physics_process(_delta):
	# sync movement and rotation with arvr camera
	translation.x = get_parent().get_node("ARVROrigin/ARVRCamera").translation.x
	translation.z = get_parent().get_node("ARVROrigin/ARVRCamera").translation.z
	rotation.y = get_parent().get_node("ARVROrigin/ARVRCamera").rotation.y
