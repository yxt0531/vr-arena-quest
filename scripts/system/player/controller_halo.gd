extends Spatial

export var rotation_speed = 2
export var controller_id = 1

var followed_controller

func _ready():
	followed_controller = null
	
	while followed_controller == null:
		match controller_id:
			1:
				followed_controller = get_parent().find_node("LeftTouchController")
			2:
				followed_controller = get_parent().find_node("RightTouchController")

func _physics_process(delta):
	translation = followed_controller.translation
	rotation = followed_controller.rotation

	match controller_id:
		1:
			$ControllerHalo.rotate_y(rotation_speed * delta)
		2:
			$ControllerHalo.rotate_y(-rotation_speed * delta)
