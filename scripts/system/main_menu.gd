extends Spatial

const BG_VOLUME_INCREASE_SPEED = 20
const EMISSION_LIGHTUP_SPEED = 0.1

var activated

func _ready():
	# var init
	activated = false
	$ControlRoom/ControlRoomDecorations/Platform.get_surface_material(1).emission_energy = 0
	$ControlRoom/ControlRoomDecorations/Portal.get_surface_material(1).emission_energy = 0
	
	NetworkPeerController.damage_multiplier = 1
	# var init end

func _process(delta):
	if not activated:
		if $Player.current_weapon_right != null:
			$ControlRoom/CeilingLight.activate_light()
			$PowerUpPlayer.play()
			$BackgroundPlayer.play()
			$PortalHalo.activate()
			$TrainingRoomBGMPlayer.play()
			
			activated = true
	else:
		if $BackgroundPlayer.volume_db < 0:
			$BackgroundPlayer.volume_db += delta * BG_VOLUME_INCREASE_SPEED
			
		if $ControlRoom/ControlRoomDecorations/Platform.get_surface_material(1).emission_energy < 1:
			$ControlRoom/ControlRoomDecorations/Platform.get_surface_material(1).emission_energy += delta * EMISSION_LIGHTUP_SPEED
		else:
			$ControlRoom/ControlRoomDecorations/Platform.get_surface_material(1).emission_energy = 1
			
		if $ControlRoom/ControlRoomDecorations/Portal.get_surface_material(1).emission_energy < 1:
			$ControlRoom/ControlRoomDecorations/Portal.get_surface_material(1).emission_energy += delta * EMISSION_LIGHTUP_SPEED
		else:
			$ControlRoom/ControlRoomDecorations/Portal.get_surface_material(1).emission_energy = 1
			
func on_input_keyboard_just_pressed(key):
	match $"3DUISwitch".current_scene:
		0:
			$"3DUISwitch/Screen0/ScreenMain/3DTextbox".append(key)
		1:
			return
		2:
			$"3DUISwitch/Screen2/ScreenOptions/3DTextbox".append(key)
			SettingsSystem.nickname = $"3DUISwitch/Screen2/ScreenOptions/3DTextbox".text
