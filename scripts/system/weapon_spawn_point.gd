extends Spatial

export var weapon: int = 0
export var weapon_rotation_speed = 5
export var spawn_delay = 10

var is_active
var spawn_timer

func _ready():
	# var init
	is_active = true
	spawn_timer = 0
	# var init end
	
	match weapon:
		Helper.WEAPON.AMMO:
			$WeaponModelPosition.add_child(load("res://scenes/weapons/ammo_model_only.tscn").instance())
		Helper.WEAPON.HANDGUN:
			$WeaponModelPosition.add_child(load("res://scenes/weapons/handgun_model_only.tscn").instance())
		Helper.WEAPON.SHOTGUN:
			$WeaponModelPosition.add_child(load("res://scenes/weapons/shotgun_model_only.tscn").instance())
		Helper.WEAPON.SMG:
			$WeaponModelPosition.add_child(load("res://scenes/weapons/smg_model_only.tscn").instance())
		_:
			print("invalid weapon for weapon spawn point")
			queue_free()

func _process(delta):
	if is_active:
		$WeaponModelPosition.rotate_y(weapon_rotation_speed * delta)
	else:
		spawn_timer += delta
		if spawn_timer >= spawn_delay:
			activate()

func _on_PlayerTriggerArea_body_entered(body):
	if is_active:
		if body.name == "Player":
			print("player entered weapon spawn point")
			
			body.available_weapon_for_swap = weapon
			body.current_occupied_weapon_spawn_point = self
			
			if weapon == Helper.WEAPON.AMMO:
				body.swap_weapon(2)
			elif body.current_weapon_right == null:
				body.swap_weapon(2)
				
func _on_PlayerTriggerArea_body_exited(body):
	if body.name == "Player":
		print("player exited weapon spawn point")
		body.available_weapon_for_swap = null
		body.current_occupied_weapon_spawn_point = null

func deactivate(from_server = false):
	if is_active:
		spawn_timer = 0
		$WeaponModelPosition.visible = false
		is_active = false
		print("weapon spawn point deactivated")
		if not from_server:
			NetworkPeerController.boardcast_wps_deactivation(name)

func activate():
	$WeaponModelPosition.visible = true
	is_active = true
	print("weapon spawn point activated")
