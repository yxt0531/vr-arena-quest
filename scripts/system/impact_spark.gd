extends Spatial

export var lifetime = 0.25

var timer

func _ready():
	# var init
	timer = 0
	# var init end
	
	_emit_spark()
	
func _emit_spark():
	# set particle lifetime
	$CPUParticles0.lifetime = lifetime
	$CPUParticles90.lifetime = lifetime
	$CPUParticles180.lifetime = lifetime
	$CPUParticles270.lifetime = lifetime
	
	# start emitting
	$CPUParticles0.emitting = true
	$CPUParticles90.emitting = true
	$CPUParticles180.emitting = true
	$CPUParticles270.emitting = true

func _process(delta):
	# destory node when reached lifetime
	if timer > lifetime:
		queue_free()
	else:
		timer += delta
