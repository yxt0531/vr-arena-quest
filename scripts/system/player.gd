extends KinematicBody

export var dead_zone = 0.125 # radius of the dead zone
export var speed_move = 1000
export var speed_rotation = 6
export var speed_fly = 500

const MAX_AMMO = 400
const MAX_HEALTH = 100

const INIT_AMMO = 64

const DISCONNECTION_TIMEOUT = 0

var arvr_origin

var dx
var dy
var rx

var self_forward_dir
var self_strafe_dir
var view_dir
var side_dir

var move_and_slide_vector3

var current_ammo
var current_health

var current_weapon_left
var current_weapon_right
var available_weapon_for_swap
var current_occupied_weapon_spawn_point

var disconnection_timer

func _ready():
	# var init
	arvr_origin = $ARVROrigin
	
	dx = 0
	dy = 0
	rx = 0
	
	self_forward_dir = null
	self_strafe_dir = null
	view_dir = null
	side_dir = null
	
	move_and_slide_vector3 = null
	
	current_ammo = INIT_AMMO
	current_health = MAX_HEALTH
	
	current_weapon_left = null
	current_weapon_right = null
	available_weapon_for_swap = null
	current_occupied_weapon_spawn_point = null
	
	disconnection_timer = 0
	
	# var init end

	_apply_color(Helper.available_colors[SettingsAvatar.primary_color], Helper.available_colors[SettingsAvatar.accent_color], Helper.available_colors[SettingsAvatar.secondary_color], Helper.available_colors[SettingsAvatar.emissive_color])
	_apply_hud()
	
	if get_parent().get_node("PlayerSpawnPoints") != null:
		randomize()
		translation = get_parent().get_node("PlayerSpawnPoints").get_child(randi() % get_parent().get_node("PlayerSpawnPoints").get_child_count()).translation

func update_color():
	_apply_color(Helper.available_colors[SettingsAvatar.primary_color], Helper.available_colors[SettingsAvatar.accent_color], Helper.available_colors[SettingsAvatar.secondary_color], Helper.available_colors[SettingsAvatar.emissive_color])

func _apply_color(primary, accent, secondary, emissive):
	# apply color from global avatar setting
	var primary_material = SpatialMaterial.new()
	primary_material.albedo_color = primary
	primary_material.metallic = 0.5
	primary_material.roughness = 0.5
	$Hitbox/Mesh.set_surface_material(1, primary_material)
	
	var accent_material = SpatialMaterial.new()
	accent_material.albedo_color = accent
	accent_material.metallic = 0.5
	accent_material.roughness = 0.5
	$Hitbox/Mesh.set_surface_material(2, accent_material)
	
	var secondary_material = SpatialMaterial.new()
	secondary_material.albedo_color = secondary
	secondary_material.metallic = 0.5
	secondary_material.roughness = 0.5
	$Hitbox/Mesh.set_surface_material(3, secondary_material)
	
	var emissive_material = SpatialMaterial.new()
	emissive_material.albedo_color = emissive
	emissive_material.emission = emissive
	emissive_material.emission_enabled = true
	$Hitbox/Mesh.set_surface_material(0, emissive_material)

func _apply_hud():
	# $LowerHUDViewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# $UpperHUDViewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	$ARVROrigin/ARVRCamera/LowerHUD.material_override.albedo_texture = $LowerHUDViewport.get_texture()
	$ARVROrigin/ARVRCamera/UpperHUD.material_override.albedo_texture = $UpperHUDViewport.get_texture()

func _physics_process(delta):
	_process_move(delta)
	_process_rotation(delta)
	
func _process(delta):
	_update_player_network_info(delta)
	_check_current_health()
	
func _process_move(delta):
	move_and_slide_vector3 = Vector3(0, 0, 0) # reset vector3
	
	if !arvr_origin.arvr_interface:
		if Input.is_action_pressed("debug_move_backward"):
			dy = -1
		elif Input.is_action_pressed("debug_move_forward"):
			dy = 1
		else:
			dy = 0
			
		if Input.is_action_pressed("debug_move_left"):
			dx = -1
		elif Input.is_action_pressed("debug_move_right"):
			dx = 1
		else:
			dx = 0
			
		if Input.is_action_pressed("debug_fly_ascend"):
			move_and_slide_vector3.y += speed_fly
			$Hitbox/Nozzles.current_mode = $Hitbox/Nozzles.MODE.ASCENDING
		elif Input.is_action_pressed("debug_fly_descend"):
			move_and_slide_vector3.y -= speed_fly
			$Hitbox/Nozzles.current_mode = $Hitbox/Nozzles.MODE.DESCENDING
		else:
			$Hitbox/Nozzles.current_mode = $Hitbox/Nozzles.MODE.STANDBY
	else:
		# set horizontal x and y
		dx = arvr_origin.get_node("LeftTouchController").get_joystick_axis(0)
		dy = arvr_origin.get_node("LeftTouchController").get_joystick_axis(1)
		
		# set vertical y
		if $ARVROrigin/RightTouchController.is_button_pressed(1):
			move_and_slide_vector3.y += speed_fly
			$Hitbox/Nozzles.current_mode = $Hitbox/Nozzles.MODE.ASCENDING
		elif $ARVROrigin/RightTouchController.is_button_pressed(7):
			move_and_slide_vector3.y -= speed_fly
			$Hitbox/Nozzles.current_mode = $Hitbox/Nozzles.MODE.DESCENDING
		else:
			$Hitbox/Nozzles.current_mode = $Hitbox/Nozzles.MODE.STANDBY

	if (dx * dx + dy * dy > dead_zone * dead_zone):
		self_forward_dir = -transform.basis.z.normalized()
		self_strafe_dir = transform.basis.x.normalized()
		
		view_dir = self_forward_dir.rotated(Vector3.UP, arvr_origin.get_node("ARVRCamera").rotation.y)
		side_dir = self_strafe_dir.rotated(Vector3.UP, arvr_origin.get_node("ARVRCamera").rotation.y)

		# var move_vector = Vector2(dx, dy).normalized() * speed_move

		# transform.origin += view_dir * dy * speed_move * delta
		# transform.origin += side_dir * dx * speed_move * delta
		
		# apply horizontal movement to move and slide vector
		move_and_slide_vector3 += ((view_dir * dy * speed_move) + (side_dir * dx * speed_move))
		
	move_and_slide(move_and_slide_vector3 * delta)

func _process_rotation(delta):
	if !arvr_origin.arvr_interface:
		if Input.is_action_pressed("debug_rotate_left") and not Input.is_action_pressed("debug_rotate_cam_left"):
			rx = 1
		elif Input.is_action_pressed("debug_rotate_right") and not Input.is_action_pressed("debug_rotate_cam_right"):
			rx = -1
		else:
			rx = 0
		
		if Input.is_action_pressed("debug_rotate_cam_left"):
			arvr_origin.get_node("ARVRCamera").rotate_y(delta * speed_rotation)
		elif Input.is_action_pressed("debug_rotate_cam_right"):
			arvr_origin.get_node("ARVRCamera").rotate_y(-delta * speed_rotation)
	else:
		rx = -arvr_origin.get_node("RightTouchController").get_joystick_axis(0)
		
	if abs(rx) > dead_zone:
		rotate_y((rx - dead_zone) * speed_rotation * delta)

func swap_weapon(hand):
	# prevent swap to the same weapon
	match hand:
		1:
			if current_weapon_left == available_weapon_for_swap:
				print("weapon already aquired, cannot swap")
				return
		2:
			if current_weapon_right == available_weapon_for_swap:
				print("weapon already aquired, cannot swap")
				return
		_:
			print("error swap weapon")
			return
	
	match available_weapon_for_swap:
		Helper.WEAPON.AMMO:
			if _add_ammo():
				available_weapon_for_swap = null
				current_occupied_weapon_spawn_point.deactivate()
		Helper.WEAPON.HANDGUN:
			if hand == 1:
				_remove_current_weapon(1)
				$ARVROrigin/LeftTouchController.add_child(DynamicResources.handgun.instance())
				current_weapon_left = Helper.WEAPON.HANDGUN
			else:
				_remove_current_weapon(2)
				$ARVROrigin/RightTouchController.add_child(DynamicResources.handgun.instance())
				current_weapon_right = Helper.WEAPON.HANDGUN
			available_weapon_for_swap = null
			current_occupied_weapon_spawn_point.deactivate()
		Helper.WEAPON.SHOTGUN:
			if hand == 1:
				_remove_current_weapon(1)
				$ARVROrigin/LeftTouchController.add_child(DynamicResources.shotgun.instance())
				current_weapon_left = Helper.WEAPON.SHOTGUN
			else:
				_remove_current_weapon(2)
				$ARVROrigin/RightTouchController.add_child(DynamicResources.shotgun.instance())
				current_weapon_right = Helper.WEAPON.SHOTGUN
			available_weapon_for_swap = null
			current_occupied_weapon_spawn_point.deactivate()
		Helper.WEAPON.SMG:
			if hand == 1:
				_remove_current_weapon(1)
				$ARVROrigin/LeftTouchController.add_child(DynamicResources.smg.instance())
				current_weapon_left = Helper.WEAPON.SMG
			else:
				_remove_current_weapon(2)
				$ARVROrigin/RightTouchController.add_child(DynamicResources.smg.instance())
				current_weapon_right = Helper.WEAPON.SMG
			available_weapon_for_swap = null
			current_occupied_weapon_spawn_point.deactivate()
		_:
			print("error swap weapon")
			
func _remove_current_weapon(hand):
	if hand == 1:
		# remove left hand weapon
		for weapon in $ARVROrigin/LeftTouchController.get_children():
			weapon.queue_free()
	else:
		# remove right hand weapon
		for weapon in $ARVROrigin/RightTouchController.get_children():
			weapon.queue_free()

func _add_ammo(amount = 32):
	if current_ammo == MAX_AMMO:
		return false
	elif (current_ammo + amount) > MAX_AMMO:
		amount = MAX_AMMO - current_ammo
		current_ammo += amount
		$ARVROrigin/ARVRCamera/SoundFXPlayer.stream = DynamicResources.sound_ammo_picked_up
		$ARVROrigin/ARVRCamera/SoundFXPlayer.play()
		print(str(amount) + " ammo added")
	else:
		current_ammo += amount
		$ARVROrigin/ARVRCamera/SoundFXPlayer.stream = DynamicResources.sound_ammo_picked_up
		$ARVROrigin/ARVRCamera/SoundFXPlayer.play()
		print(str(amount) + " ammo added")
	return true

func _update_player_network_info(delta):
	NetworkPeerController.player.current_map = get_parent().name
	
	NetworkPeerController.player.nickname = SettingsSystem.nickname
	
	NetworkPeerController.player.weapon_accent_color = SettingsWeapon.accent_color
	NetworkPeerController.player.weapon_laser_color = SettingsWeapon.laser_color
	
	NetworkPeerController.player.avatar_primary_color = SettingsAvatar.primary_color
	NetworkPeerController.player.avatar_secondary_color = SettingsAvatar.secondary_color
	NetworkPeerController.player.avatar_accent_color = SettingsAvatar.accent_color
	NetworkPeerController.player.avatar_emissive_color = SettingsAvatar.emissive_color
	
	NetworkPeerController.player.translation = translation
	NetworkPeerController.player.rotation_degrees = rotation_degrees
	
	NetworkPeerController.player.hitbox_translation = $Hitbox.translation
	NetworkPeerController.player.hitbox_rotation_degrees = $Hitbox.rotation_degrees
	
	NetworkPeerController.player.left_weapon = current_weapon_left
	NetworkPeerController.player.left_weapon_translation = $ARVROrigin/LeftTouchController.translation
	NetworkPeerController.player.left_weapon_rotation_degrees = $ARVROrigin/LeftTouchController.rotation_degrees
	
	if current_weapon_left != null:
		NetworkPeerController.player.left_weapon_laser_on = $ARVROrigin/LeftTouchController.get_child(0).get_node("LaserPointer").visible
	
	NetworkPeerController.player.right_weapon = current_weapon_right
	NetworkPeerController.player.right_weapon_translation = $ARVROrigin/RightTouchController.translation
	NetworkPeerController.player.right_weapon_rotation_degrees = $ARVROrigin/RightTouchController.rotation_degrees
	
	if current_weapon_right != null:
		NetworkPeerController.player.right_weapon_laser_on = $ARVROrigin/RightTouchController.get_child(0).get_node("LaserPointer").visible

	if get_parent().name != "MainMenu":
		if not NetworkPeerController.is_connected_ok:
			disconnection_timer += delta
			if disconnection_timer >= DISCONNECTION_TIMEOUT:
				Helper.async_loader.debug_load(Helper.MAP.MAIN_MENU)
				disconnection_timer = -1000
		else:
			disconnection_timer = 0

func _check_current_health():
	if current_health <= 0 and get_parent().get_node("PlayerSpawnPoints") != null:
		# player die
		# boardcast die here
		NetworkPeerController.boardcast_kill(translation)
		_reset()

func _reset():
	randomize()
	translation = get_parent().get_node("PlayerSpawnPoints").get_child(randi() % get_parent().get_node("PlayerSpawnPoints").get_child_count()).translation
	
	current_health = MAX_HEALTH
	current_ammo = INIT_AMMO

	if $ARVROrigin/LeftTouchController.get_child_count() > 0:
		for child in $ARVROrigin/LeftTouchController.get_children():
			child.queue_free()
			
	if $ARVROrigin/RightTouchController.get_child_count() > 0:
		for child in $ARVROrigin/RightTouchController.get_children():
			child.queue_free()
	
	current_weapon_left = null
	current_weapon_right = null
	available_weapon_for_swap = null
	current_occupied_weapon_spawn_point = null
