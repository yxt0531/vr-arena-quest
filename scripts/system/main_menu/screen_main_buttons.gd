extends Area

func hit():
	match name:
		"ButtonJoin":
			if Helper.is_valid_connection_socket(get_parent().get_node("3DTextbox").text):
				find_parent("MainMenu").get_node("3DUISwitch").change_scene(1)
				find_parent("MainMenu").get_node("3DUISwitch/Screen1/ScreenJoin/TextJoiningServer").visible = true
				find_parent("MainMenu").get_node("3DUISwitch/Screen1/ScreenJoin/TextJoiningServerFailed").visible = false
				find_parent("MainMenu").get_node("3DUISwitch/Screen1/ScreenJoin/TestServerDisconnected").visible = false
				
				SettingsSystem.connection_address = Helper.get_ip_from_socket(get_parent().get_node("3DTextbox").text)
				SettingsSystem.connection_port = Helper.get_port_from_socket(get_parent().get_node("3DTextbox").text)
				NetworkPeerController.connect_to(SettingsSystem.connection_address, SettingsSystem.connection_port)
				SettingsSystem.save()
		"ButtonOptions":
			find_parent("MainMenu").get_node("3DUISwitch").change_scene(2)
		"ButtonExit":
			find_parent("MainMenu").get_node("3DUISwitch").change_scene(3)
