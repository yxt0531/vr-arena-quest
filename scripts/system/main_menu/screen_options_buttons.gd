extends Area

func hit():
	match name:
		"ButtonReturn":
			find_parent("MainMenu").get_node("3DUISwitch").change_scene(0)
			SettingsAvatar.save()
			SettingsWeapon.save()
			SettingsSystem.save()
		"ButtonLaserColor":
			if SettingsWeapon.laser_color == Helper.available_color_count - 1:
				SettingsWeapon.laser_color = 0
			else:
				SettingsWeapon.laser_color += 1
		"ButtonWeaponColor":
			if SettingsWeapon.accent_color == Helper.available_color_count - 1:
				SettingsWeapon.accent_color = 0
			else:
				SettingsWeapon.accent_color += 1
		"ButtonDefaultLaser":
			SettingsWeapon.laser_on_by_default = not SettingsWeapon.laser_on_by_default
		"ButtonAvatarPrimary":
			if SettingsAvatar.primary_color == Helper.available_color_count - 1:
				SettingsAvatar.primary_color = 0
			else:
				SettingsAvatar.primary_color += 1
		"ButtonAvatarSecondary":
			if SettingsAvatar.secondary_color == Helper.available_color_count - 1:
				SettingsAvatar.secondary_color = 0
			else:
				SettingsAvatar.secondary_color += 1
		"ButtonAvatarAccent":
			if SettingsAvatar.accent_color == Helper.available_color_count - 1:
				SettingsAvatar.accent_color = 0
			else:
				SettingsAvatar.accent_color += 1
		"ButtonAvatarEmissive":
			if SettingsAvatar.emissive_color == Helper.available_color_count - 1:
				SettingsAvatar.emissive_color = 0
			else:
				SettingsAvatar.emissive_color += 1
	
	get_parent().update_ui()
	get_parent().update_objects()
