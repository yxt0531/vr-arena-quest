extends Spatial

const HALO_SPEED = 1

export var teleport_to = Vector3(0, 0, 0)
export var auto_active = false

var halo_material
var is_active

func _ready():
	halo_material = $PortalHalo.get_surface_material(0)
	visible = false
	is_active = false

	if auto_active:
		activate()
		
func _process(delta):
	if is_active:
		if halo_material.uv1_offset.x < 1:
			halo_material.uv1_offset.x += delta * HALO_SPEED
		else:
			halo_material.uv1_offset.x = 0

func activate():
	visible = true
	is_active = true

func _on_TriggerArea_body_entered(body):
	if is_active:
		body.translation = teleport_to
