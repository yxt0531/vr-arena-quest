extends Spatial

export var max_length = 50

const CHAR_SPACE = 0.07

var text
var length

var offset

func _ready():
	# var init
	text = ""
	length = 0
	
	offset = translation.x
	# var init end
	
func append(letter):
	var new_letter

	if letter != "#" and length >= max_length:
		return

	if letter == ":":
		new_letter = $Letters/Colon.duplicate()
		text += letter
	elif letter == ".":
		new_letter = $Letters/Dot.duplicate()
		text += letter
	elif letter == " ":
		new_letter = $Letters/Space.duplicate()
		text += letter
	elif letter == "#":
		# print("remove letter from textbox")
		text = text.substr(0, text.length() - 1)
	else:
		new_letter = $Letters.find_node(letter).duplicate()
		text += letter
		
	if new_letter != null:
		new_letter.translation.x = CHAR_SPACE * length
		add_child(new_letter)
		length += 1
	elif length > 0:
		get_child(get_child_count() - 1).queue_free()
		length -= 1
		
	translation.x = (-(CHAR_SPACE * length * scale.x) / 2) + offset

func set_text(new_text):
	clear()
	if new_text != "":
		var current_char_index = 0
		while current_char_index < new_text.length():
			append(new_text[current_char_index])
			current_char_index += 1
			
func clear():
	translation.x = offset
	
	for letter in get_children():
		if letter.name != "Letters":
			letter.queue_free()
	text = ""
	length = 0
