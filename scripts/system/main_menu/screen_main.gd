extends Spatial

var default_button_material
var greyed_out_button_material

func _ready():
	# var init
	default_button_material = DynamicResources.material_emissive_white_extra_dim
	greyed_out_button_material = DynamicResources.material_plastic_matte_black
	# var init end
	
	if Helper.is_valid_connection_socket(SettingsSystem.connection_address + ":" + str(SettingsSystem.connection_port)):
		$"3DTextbox".set_text(SettingsSystem.connection_address + ":" + str(SettingsSystem.connection_port))
	
func _process(_delta):
	if Helper.is_valid_connection_socket($"3DTextbox".text):
		$ButtonJoin/TextJoin.set_surface_material(0, default_button_material)
	else:
		$ButtonJoin/TextJoin.set_surface_material(0, greyed_out_button_material)
