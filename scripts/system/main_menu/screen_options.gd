extends Spatial

func _ready():
	update_ui()

func update_ui():
	$"3DTextbox".set_text(SettingsSystem.nickname)
	if SettingsWeapon.laser_on_by_default:
		$ButtonDefaultLaser/TextDefaultLaserOn.visible = true
		$ButtonDefaultLaser/TextDefaultLaserOff.visible = false
	else:
		$ButtonDefaultLaser/TextDefaultLaserOn.visible = false
		$ButtonDefaultLaser/TextDefaultLaserOff.visible = true
	_apply_colors_to_texts()

func _apply_colors_to_texts():
	$ButtonLaserColor/TextLaserColor.get_surface_material(0).albedo_color = Helper.available_colors[SettingsWeapon.laser_color]
	$ButtonLaserColor/TextLaserColor.get_surface_material(0).emission = Helper.available_colors[SettingsWeapon.laser_color]

	$ButtonWeaponColor/TextWeaponColor.get_surface_material(0).albedo_color = Helper.available_colors[SettingsWeapon.accent_color]
	$ButtonWeaponColor/TextWeaponColor.get_surface_material(0).emission = Helper.available_colors[SettingsWeapon.accent_color]
	
	$ButtonAvatarPrimary/TextAvatarPrimary.get_surface_material(0).albedo_color = Helper.available_colors[SettingsAvatar.primary_color]
	$ButtonAvatarPrimary/TextAvatarPrimary.get_surface_material(0).emission = Helper.available_colors[SettingsAvatar.primary_color]
	
	$ButtonAvatarSecondary/TextAvatarSecondary.get_surface_material(0).albedo_color = Helper.available_colors[SettingsAvatar.secondary_color]
	$ButtonAvatarSecondary/TextAvatarSecondary.get_surface_material(0).emission = Helper.available_colors[SettingsAvatar.secondary_color]
	
	$ButtonAvatarAccent/TextAvatarAccent.get_surface_material(0).albedo_color = Helper.available_colors[SettingsAvatar.accent_color]
	$ButtonAvatarAccent/TextAvatarAccent.get_surface_material(0).emission = Helper.available_colors[SettingsAvatar.accent_color]
	
	$ButtonAvatarEmissive/TextAvatarEmissive.get_surface_material(0).albedo_color = Helper.available_colors[SettingsAvatar.emissive_color]
	$ButtonAvatarEmissive/TextAvatarEmissive.get_surface_material(0).emission = Helper.available_colors[SettingsAvatar.emissive_color]

func update_objects():
	find_parent("MainMenu").get_node("Player").update_color()
	if find_parent("MainMenu").get_node("Player/ARVROrigin/LeftTouchController").get_child_count() > 0:
		find_parent("MainMenu").get_node("Player/ARVROrigin/LeftTouchController").get_child(0).update_color()
	if find_parent("MainMenu").get_node("Player/ARVROrigin/RightTouchController").get_child_count() > 0:
		find_parent("MainMenu").get_node("Player/ARVROrigin/RightTouchController").get_child(0).update_color()
