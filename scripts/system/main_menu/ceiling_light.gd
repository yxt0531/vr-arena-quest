extends Spatial

var TARGET_MATERIAL_ENERGY = 2
var TARGET_LIGHT_ENERGY = 1

var activated

export var activate_speed = 0.5

func _ready():
	# var init
	activated = false
	# var init end
	
	$CeilingLight.get_surface_material(2).emission_energy = 0.5
	$OmniLight.light_energy = 0.25

func _process(delta):
	if activated:
		if $CeilingLight.get_surface_material(2).emission_energy < TARGET_MATERIAL_ENERGY:
			$CeilingLight.get_surface_material(2).emission_energy += delta * 2 * activate_speed
		else:
			$CeilingLight.get_surface_material(2).emission_energy = TARGET_MATERIAL_ENERGY
			
		if $OmniLight.light_energy < TARGET_LIGHT_ENERGY:
			$OmniLight.light_energy += delta * activate_speed
		else:
			$OmniLight.light_energy = TARGET_LIGHT_ENERGY
			
func activate_light():
	activated = true
	print("ceiling light activated")
