extends Spatial

const KEY_HISTORY_LENGTH = 20

var current_keypress
var pop_buffer

var key_history

func _ready():
	# var init
	current_keypress = null
	pop_buffer = null
	
	key_history = ""
	# var init end
	
func set_current_keypress(key):
	current_keypress = key
	key_history += key
	
	if key_history.length() > KEY_HISTORY_LENGTH:
		key_history = key_history.substr(1) # keep key history less than max length
		
	print("keyboard current keypress set to: " + str(key))
	
	# _process_command() # disable this on release
	
	# hook up input here
	if find_parent("MainMenu") != null:
		find_parent("MainMenu").on_input_keyboard_just_pressed(pop_keypress())
	elif find_parent("DebugRoom1") != null:
		find_parent("DebugRoom1").on_input_keyboard_just_pressed(pop_keypress())
	
func pop_keypress():
	pop_buffer = current_keypress
	current_keypress = null
	
	return pop_buffer

func _process_command():
	# call every keypress
	if find_parent("MainMenu") != null:
		if key_history.find(":DB1:") != -1:
			find_parent("MainMenu").get_node("Player/UpperHUDViewport/UpperHUDUI/LabelInfo").text = "LOADING DB1"
			find_parent("MainMenu").get_node("Player/UpperHUDViewport/UpperHUDUI/LabelInfo").visible = true
			find_parent("MainMenu").get_node("AsyncLoader").debug_load("res://scenes/debug/debug_room_1.tscn")
			
			key_history = ""
		elif key_history.find(":201:") != -1:
			find_parent("MainMenu").get_node("3DUISwitch/Screen0/ScreenMain/3DTextbox").set_text("192.168.0.201:23331")
			find_parent("MainMenu").get_node("3DUISwitch/Screen0/ScreenMain/ButtonJoin").hit()
