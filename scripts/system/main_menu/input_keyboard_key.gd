extends Area

const KEY_TRAVEL_DISTANCE = 0.005
const KEY_RECOVER_SPEED = 0.03

var key

var original_base_material
var highlight_base_material

func _ready():
	key = name
	
	if key == "Colon":
		key = ":"
	elif key == "Dot":
		key = "."
	elif key == "Space":
		key = " "
	elif key == "Backspace":
		key = "#"
		
	original_base_material = DynamicResources.material_plastic_matte_white_floral
	highlight_base_material = DynamicResources.material_emissive_white_dim
	
func _physics_process(delta):
	if translation.y < 0:
		translation.y += delta * KEY_RECOVER_SPEED
	else:
		translation.y = 0
		if key == " ":
			get_child(0).set_surface_material(0, original_base_material)
		else:
			get_child(0).set_surface_material(1, original_base_material)

func hit():
	# print(key + " key hit")
	translation.y = -KEY_TRAVEL_DISTANCE

	if key == " ":
		get_child(0).set_surface_material(0, highlight_base_material)
	else:
		get_child(0).set_surface_material(1, highlight_base_material)

	get_parent().set_current_keypress(key)
