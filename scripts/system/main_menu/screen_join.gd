extends Spatial

func _process(_delta):
	if NetworkPeerController.is_connection_failed:
		$TextJoiningServer.visible = false
		$TextJoiningServerFailed.visible = true
	elif NetworkPeerController.is_connected_ok:
		$ButtonReturn/TextReturn.visible = false
