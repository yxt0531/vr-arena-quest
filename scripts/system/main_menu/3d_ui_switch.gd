extends Spatial

enum DIRECTION {
	LEFT = 1,
	RIGHT = 2
}

const ROTATION_SPEED = 150

var is_changing_scene
var current_scene

var rotation_speed_multiplier

func _ready():
	# var init
	is_changing_scene = false
	current_scene = 0
	
	rotation_speed_multiplier = 0
	# var init end

func _process(delta):		
	if (current_scene * 90) % 360 != rotation_degrees.y:
		if rotation_degrees.y < ((current_scene * 90) % 360):
			rotation_degrees.y += (delta * rotation_speed_multiplier * ROTATION_SPEED)
			if rotation_degrees.y > ((current_scene * 90) % 360):
				rotation_degrees.y = ((current_scene * 90) % 360)
		elif rotation_degrees.y > ((current_scene * 90) % 360):
			rotation_degrees.y -= (delta * rotation_speed_multiplier * ROTATION_SPEED)
			if rotation_degrees.y < ((current_scene * 90) % 360):
				rotation_degrees.y = ((current_scene * 90) % 360)
			
func change_scene(scene_id):
	current_scene = scene_id
	rotation_speed_multiplier = abs(((current_scene * 90) % 360 - rotation_degrees.y) / 90)
