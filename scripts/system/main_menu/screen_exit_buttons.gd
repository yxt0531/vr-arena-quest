extends Area

const EXIT_DELAY = 0.25 # stop controller viberation

var exit_timer
var exiting

func _ready():
	exit_timer = 0
	exiting = false
	
func _process(delta):
	if exiting:
		if exit_timer < EXIT_DELAY:
			exit_timer += delta
		else:
			exiting = false
			get_tree().quit()

func hit():
	match name:
		"ButtonNo":
			find_parent("MainMenu").get_node("3DUISwitch").change_scene(0)
		"ButtonYes":
			exiting = true
			# get_tree().quit()
