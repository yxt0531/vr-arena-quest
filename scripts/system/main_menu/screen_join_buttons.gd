extends Area

func hit():
	match name:
		"ButtonReturn":
			if not NetworkPeerController.is_connected_ok:
				find_parent("MainMenu").get_node("3DUISwitch").change_scene(0)
				get_parent().get_node("TextJoiningServer").visible = false
				get_parent().get_node("TextJoiningServerFailed").visible = false
				get_parent().get_node("TestServerDisconnected").visible = false
				NetworkPeerController.disconnect_fron_server()
