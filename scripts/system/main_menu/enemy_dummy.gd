extends Spatial

export var MOVEMENT_SPEED = 5

var reverse_direction

func _ready():
	reverse_direction = false

func _process(delta):
	if reverse_direction:
		translation.x -= delta * MOVEMENT_SPEED
	else:
		translation.x += delta * MOVEMENT_SPEED
		
	if translation.x >= 18:
		reverse_direction = true
	elif translation.x <= 12:
		reverse_direction = false
