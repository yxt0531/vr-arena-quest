extends Spatial

var nickname

var weapon_accent_color
var weapon_laser_color

var avatar_primary_color
var avatar_secondary_color
var avatar_accent_color
var avatar_emissive_color

var left_weapon
var left_weapon_laser_on

var right_weapon
var right_weapon_laser_on

var left_weapon_prev
var right_weapon_prev

func _ready():
	# var init
	left_weapon_prev = null
	right_weapon_prev = null
	# var init end

func update_color():
	$Hitbox/Mesh.get_surface_material(1).albedo_color = Helper.available_colors[avatar_primary_color]
	$Hitbox/Mesh.get_surface_material(2).albedo_color = Helper.available_colors[avatar_secondary_color]
	$Hitbox/Mesh.get_surface_material(3).albedo_color = Helper.available_colors[avatar_accent_color]
	$Hitbox/Mesh.get_surface_material(0).albedo_color = Helper.available_colors[avatar_emissive_color]
	$Hitbox/Mesh.get_surface_material(0).emission = Helper.available_colors[avatar_emissive_color]

func _process(_delta):
	# print(right_weapon)
	if left_weapon != left_weapon_prev:
		# left weapon changed
		print("enemy left weapon changed")
		
		for child in $LeftWeaponPos.get_children():
			if child.name != "ControllerHalo":
				child.queue_free()
		
		match left_weapon:
			Helper.WEAPON.HANDGUN:
				$LeftWeaponPos.add_child(DynamicResources.handgun_enemy.instance())
			Helper.WEAPON.SHOTGUN:
				$LeftWeaponPos.add_child(DynamicResources.shotgun_enemy.instance())
			Helper.WEAPON.SMG:
				$LeftWeaponPos.add_child(DynamicResources.smg_enemy.instance())
		left_weapon_prev = left_weapon
		
	if right_weapon != right_weapon_prev:
		# right weapon changed
		print("enemy right weapon changed")
		
		for child in $RightWeaponPos.get_children():
			if child.name != "ControllerHalo":
				child.queue_free()
		
		match right_weapon:
			Helper.WEAPON.HANDGUN:
				$RightWeaponPos.add_child(DynamicResources.handgun_enemy.instance())
			Helper.WEAPON.SHOTGUN:
				$RightWeaponPos.add_child(DynamicResources.shotgun_enemy.instance())
			Helper.WEAPON.SMG:
				$RightWeaponPos.add_child(DynamicResources.smg_enemy.instance())
		right_weapon_prev = right_weapon
	
