extends Node

var player
var enemies

var peer

var is_connected_ok
var is_connection_failed

var enemy_controller

remote var damage_multiplier

func _ready():
	# var init
	player = {
		"current_map": null,
		
		"nickname": null,
		
		"weapon_accent_color": null,
		"weapon_laser_color": null,
		
		"avatar_primary_color": null,
		"avatar_secondary_color": null,
		"avatar_accent_color": null,
		"avatar_emissive_color": null,
		
		"translation": null,
		"rotation_degrees": null,
		
		"hitbox_translation": null,
		"hitbox_rotation_degrees": null,
		
		"left_weapon": null,
		"left_weapon_translation": null,
		"left_weapon_rotation_degrees": null,
		"left_weapon_laser_on": null,
		
		"right_weapon": null,
		"right_weapon_translation": null,
		"right_weapon_rotation_degrees": null,
		"right_weapon_laser_on": null
	}
	
	enemies = {}
	
	peer = NetworkedMultiplayerENet.new()
	is_connected_ok = false
	is_connection_failed = false
	
	enemy_controller = null
	
	damage_multiplier = 1
	
	# var init end

	# signal connect
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	# signal connect end

func _process(_delta):
	if is_connected_ok:
		rpc_unreliable_id(1, "push_player", player) # update self stats to server
	# print(damage_multiplier)
	
func _connected_ok():
	is_connected_ok = true
	is_connection_failed = false
	print("network connected")
	
func _server_disconnected():
	is_connected_ok = false
	is_connection_failed = false
	print("connection closed by server")
	
func _connected_fail():
	is_connected_ok = false
	is_connection_failed = true
	print("network connection failed")

func connect_to(address, port):
	peer = NetworkedMultiplayerENet.new()
	is_connected_ok = false
	is_connection_failed = false
	peer.create_client(address, port)
	get_tree().set_network_peer(NetworkPeerController.peer)
	print("connecting to: " + address + ":" + str(port))

func disconnect_fron_server():
	# deprecated
	disconnect_from_server()

func disconnect_from_server():
	if is_connected_ok and not is_connection_failed:
		peer.close_connection()
		is_connected_ok = false
		is_connection_failed = false
		print("connection closed by self")

remote func load_map(map):
	if Helper.MAP.has(map):
		Helper.async_loader.debug_load(Helper.MAP[map])
		print("loading server map: " + map)

remote func push_enemies(enemies_from_server):
	enemies = enemies_from_server
	# print(enemies.size())

func boardcast_fire_packet(packet):
	if is_connected_ok and not is_connection_failed:
		packet.id = get_tree().get_network_unique_id()
		rpc_id(1, "boardcast_fire_packet_received", packet)

remote func fire_packet_received(packet):
	print("fire packet received")
	if enemy_controller != null:
		enemy_controller.generate_enemy_fire_objects(packet)

remote func hit_received(dmg):
	print("hit recelived with dmg: " + str(dmg))
	if enemy_controller != null:
		enemy_controller.hit_received(dmg)
	
func push_hit(id, dmg):
	rpc_id(int(id), "hit_received", dmg)

func boardcast_kill(death_position):
	rpc_id(1, "boardcast_kill_received", SettingsSystem.nickname, death_position)

remote func kill_received(nickname, death_position):
	print(nickname + " is killed")
	if enemy_controller != null:
		enemy_controller.kill_received(nickname, death_position)

func boardcast_wps_deactivation(wps_name):
	rpc_id(1, "boardcast_wps_deactivation_received", wps_name)

remote func wps_deactivation_received(wps_name):
	if enemy_controller != null:
		enemy_controller.wps_deactivation_received(wps_name)
