extends Node

var trigger_threshold = 0.25
var grip_threshold = 0.8

var laser_color = 10
var laser_on_by_default = true

var accent_color = 4

const SAVE_PATH = "user://weapon.setting"

var save_file
var settings = {}

func _init():
	save_file = File.new()
	_load()

func save():
	settings = {
		"laser_color": laser_color,
		"laser_on_by_default": laser_on_by_default,
		"accent_color": accent_color,
	}
	
	save_file.open(SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(settings))
	save_file.close()

func _load():
	if save_file.file_exists(SAVE_PATH):
		save_file.open(SAVE_PATH, File.READ)
		settings = parse_json(save_file.get_line())
		save_file.close()
		
		laser_color = settings.laser_color
		laser_on_by_default = settings.laser_on_by_default
		accent_color = settings.accent_color
