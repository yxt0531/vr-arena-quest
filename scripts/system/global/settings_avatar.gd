extends Node

var primary_color = 12
var secondary_color = 4
var accent_color = 13
var emissive_color = 5

const SAVE_PATH = "user://avatar.setting"

var save_file
var settings = {}

func _init():
	save_file = File.new()
	_load()

func save():
	settings = {
		"primary_color": primary_color,
		"secondary_color": secondary_color,
		"accent_color": accent_color,
		"emissive_color": emissive_color
	}
	
	save_file.open(SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(settings))
	save_file.close()

func _load():
	if save_file.file_exists(SAVE_PATH):
		save_file.open(SAVE_PATH, File.READ)
		settings = parse_json(save_file.get_line())
		save_file.close()
		
		primary_color = settings.primary_color
		secondary_color = settings.secondary_color
		accent_color = settings.accent_color
		emissive_color = settings.emissive_color

