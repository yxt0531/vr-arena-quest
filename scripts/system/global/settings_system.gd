extends Node

var nickname = "PLAYER1"
var connection_address = ""
var connection_port = ""

const SAVE_PATH = "user://system.setting"

var save_file
var settings = {}

func _init():
	save_file = File.new()
	_load()

func save():
	settings = {
		"nickname": nickname,
		"connection_address": connection_address,
		"connection_port": connection_port,
	}
	
	save_file.open(SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(settings))
	save_file.close()

func _load():
	if save_file.file_exists(SAVE_PATH):
		save_file.open(SAVE_PATH, File.READ)
		settings = parse_json(save_file.get_line())
		save_file.close()
		
		nickname = settings.nickname
		connection_address = settings.connection_address
		connection_port = settings.connection_port
