extends Node

# scenes that need to be dynamically instanced

var explosion
var enemy
var impact_spark
var bullet_impact_concrete_brick_sound

var handgun
var handgun_enemy

var shotgun
var shotgun_enemy

var smg
var smg_enemy

var material_plastic_matte_white_floral
var material_plastic_matte_black
var material_emissive_white_dim
var material_emissive_white_extra_dim

var sound_ammo_picked_up

func _ready():
	explosion = preload("res://scenes/maps/utils/explosion.tscn")
	enemy = preload("res://scenes/system/enemy.tscn")
	impact_spark = preload("res://scenes/system/impact_spark.tscn")
	bullet_impact_concrete_brick_sound = preload("res://scenes/weapons/components/bullet_impact_concrete_brick_sound.tscn")
	
	handgun = preload("res://scenes/weapons/handgun.tscn")
	handgun_enemy = preload("res://scenes/weapons/handgun_enemy.tscn")
	shotgun = preload("res://scenes/weapons/shotgun.tscn")
	shotgun_enemy = preload("res://scenes/weapons/shotgun_enemy.tscn")
	smg = preload("res://scenes/weapons/smg.tscn")
	smg_enemy = preload("res://scenes/weapons/smg_enemy.tscn")

	material_plastic_matte_white_floral = preload("res://assets/common_materials/plastic_matte_white_floral.material")
	material_plastic_matte_black = preload("res://assets/common_materials/plastic_matte_black.material")
	material_emissive_white_dim = preload("res://assets/common_materials/emissive_white_dim.material")
	material_emissive_white_extra_dim = preload("res://assets/common_materials/emissive_white_extra_dim.material")

	sound_ammo_picked_up = preload("res://assets/weapons/ammo/sounds/ammo_picked_up.wav")
