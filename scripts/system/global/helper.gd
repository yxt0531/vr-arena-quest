extends Node

enum CONTROLLER_BUTTON {
	YB = 1,
	GRIP_TRIGGER = 2, # grip trigger pressed over threshold
	ENTER = 3, # Menu Button on left controller

	TOUCH_XA = 5,
	TOUCH_YB = 6,

	XA = 7,

	TOUCH_THUMB_UP = 10,
	TOUCH_INDEX_TRIGGER = 11,
	TOUCH_INDEX_POINTING = 12,

	THUMBSTICK = 14, # left/right thumb stick pressed
	INDEX_TRIGGER = 15, # index trigger pressed over threshold
}

enum CONTROLLER_AXIS {
	THUMBSTICK_LR = 0,
	THUMBSTICK_TB = 1,
	INDEX_TRIGGER = 2,
	GRIP_TRIGGER = 3
}

enum WEAPON {
	HANDGUN = 0,
	SHOTGUN = 1,
	SMG = 2,
	AMMO = 99
}

const MAP = {
	"MAIN_MENU": "res://scenes/system/main_menu.tscn",
	"CYBERLOBBY": "res://scenes/maps/cyberlobby.tscn",
	"PRISON": "res://scenes/maps/prison.tscn",
	"CATHEDRAL": "res://scenes/maps/cathedral.tscn"
}

var ip_regex
var ipv6_regex
var number_regex

var available_colors
var available_color_count

var async_loader

func _ready():
	# var init	
	ip_regex = RegEx.new()
	ipv6_regex = RegEx.new()
	number_regex = RegEx.new()
	
	ip_regex.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
	ipv6_regex.compile("(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))")
	number_regex.compile("^[0-9]*$")
	
	available_colors = []
	
	available_colors.append(Color.aqua)
	available_colors.append(Color.black)
	available_colors.append(Color.blue)
	available_colors.append(Color.fuchsia)
	available_colors.append(Color.gray)
	available_colors.append(Color.green)
	available_colors.append(Color.lime)
	available_colors.append(Color.maroon)
	available_colors.append(Color.navyblue)
	available_colors.append(Color.purple)
	available_colors.append(Color.red)
	available_colors.append(Color.silver)
	available_colors.append(Color.teal)
	available_colors.append(Color.white)
	available_colors.append(Color.yellow)
	
	available_color_count = available_colors.size()
	
	async_loader = null
	# var init end

func find_node_by_name(root, name): # find node from root by name
	if root.get_name() == name:
		# print("node found: " + name)
		return root
		
	for child in root.get_children():
		if child.get_name() == name:
			# print("node found: " + name)
			return child
		
		var found = find_node_by_name(child, name)
		if found:
			# print("node found: " + name)
			return found
	# print("unable to find node: " + name)
	return null

func get_fire_interval(rounds_per_minute):
	return float(60) / float(rounds_per_minute)
	
func is_ip_address(ip: String):
	if ip_regex.search(ip) or ipv6_regex.search(ip):
		return true
	else:
		return false

func is_number(input: String):
	if input == null or input == "":
		return false
	elif number_regex.search(input):
		return true
	else:
		return false

func is_valid_connection_socket(text: String):
	if text == null or text == "":
		return false
	elif text.find_last(":") == -1:
		return false
	else:
		return is_ip_address(text.substr(0, text.find_last(":"))) and is_number(text.substr(text.find_last(":") + 1))

func get_ip_from_socket(text: String):
	if is_valid_connection_socket(text):
		return text.substr(0, text.find_last(":"))
	else:
		return null
	
func get_port_from_socket(text: String):
	if is_valid_connection_socket(text):
		return int(text.substr(text.find_last(":") + 1))
	else:
		return null
