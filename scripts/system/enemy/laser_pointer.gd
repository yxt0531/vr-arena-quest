extends ImmediateGeometry

var this_enemy_instance

func _ready():
	this_enemy_instance = get_parent().get_parent().get_parent()
	
	var laser_material = SpatialMaterial.new()
	laser_material.flags_unshaded = true
	laser_material.flags_use_point_size = true
	laser_material.albedo_color = Helper.available_colors[this_enemy_instance.weapon_laser_color]
	material_override = laser_material

func _physics_process(_delta):
	match get_parent().get_parent().name:
		"LeftWeaponPos":
			if this_enemy_instance.left_weapon_laser_on:
				visible = true
			else:
				visible = false
		"RightWeaponPos":
			if this_enemy_instance.right_weapon_laser_on:
				visible = true
			else:
				visible = false
	
	if visible:
		if $RayCast.get_collider() != null:
			clear()
			begin(Mesh.PRIMITIVE_LINE_STRIP, null)
			add_vertex(Vector3(0, 0, 0))
			add_vertex(to_local($RayCast.get_collision_point()))
			end()
		else:
			clear()
			begin(Mesh.PRIMITIVE_LINE_STRIP, null)
			add_vertex(Vector3(0, 0, 0))
			add_vertex(Vector3(0, 0, -1000))
			end()
