# assign this script to root node of weapon

extends Spatial

export var continuous_fire = true
export var rounds_per_minute = 360
export var ammo_consumption = 1
export var round_multiplier = 2
export var cock_on_trigger_release = false

export var max_spread_angle = 5

export var damage = 20
export var damage_range = 2

const MUZZLE_FIRE_LIGHT_TIMEOUT = 0.04

# internal use only
var remaining_shot
var fired
var fire_timer
var muzzle_fire_light_timer

var shot_sound_count
var dry_fire_sound_count


var player

func _ready():
	# var init
	remaining_shot = 0
	fired = false
	fire_timer = 0
	muzzle_fire_light_timer = 0
	
	shot_sound_count = $ShotSounds.get_child_count()
	dry_fire_sound_count = $DryFireSounds.get_child_count()
	
	player = find_parent("Player")
	
	if ammo_consumption > 1:
		# enable multishot for laser pointer
		$LaserPointer.enable_multishot_per_round(ammo_consumption, max_spread_angle, round_multiplier)
	# var init end

func update_color():
	get_child(0).update_color()
	$LaserPointer.update_color()

func _process(delta):
	if $MuzzleFireLight.visible:
		muzzle_fire_light_timer += delta
		if muzzle_fire_light_timer >= MUZZLE_FIRE_LIGHT_TIMEOUT:
			$MuzzleFireLight.visible = false
			muzzle_fire_light_timer = 0
			
	if fired and continuous_fire:
		fire_timer += delta
		if fire_timer >= Helper.get_fire_interval(rounds_per_minute):
			fired = false
			fire_timer = 0

func toggle_laser_pressed():
	$LaserPointer.toggle_laser_pressed()
	
func toggle_laser_just_released():
	$LaserPointer.toggle_laser_just_released()

func fire_pressed(_delta):
	if not fired:
		# set fire_timer to fire time, avoid waiting for interval for first shot
		_fire()
		fired = true
	
func fire_just_released():
	if not continuous_fire or cock_on_trigger_release:
		fired = false
		fire_timer = 0
		
func _fire():
	if player.current_ammo >= ammo_consumption:
		remaining_shot = ammo_consumption
		
		# play random bullet leave barrel sound
		$BulletLeaveBarrelSound.play_random()
		
		# play random shot sound
		randomize()
		while $ShotSounds.get_child(randi() % shot_sound_count).playing:
			randomize()
		$ShotSounds.get_child(randi() % shot_sound_count).play()
		
		# flash muzzle fire light
		$MuzzleFireLight.visible = true
		
		# display discharge fire spark
		$DischargeSpark.discharge()
		
		# create impact spark
		_create_impact_spark()
	
		# create bullet impact sound
		_create_bullet_impact_concrete_brick_sound()

		# call hit function from collided object
		_process_hit()

		# remove ammo count by ammo consumption
		player.current_ammo -= ammo_consumption
		
		# rumble controller
		get_parent().rumble = 1
	else:
		# no ammo
		randomize()
		$DryFireSounds.get_child(randi() % dry_fire_sound_count).play()
	
func _create_impact_spark():
	if ammo_consumption == 1:
		# generate 1 impact spark
		if $LaserPointer.is_colliding():
			var impact_spark = DynamicResources.impact_spark.instance()
			impact_spark.translation = $LaserPointer.get_collision_point()
			get_tree().get_root().add_child(impact_spark)
	else:
		# generate multi sparks from collision points in laser pointer
		for collision_point in $LaserPointer.get_collision_points():
			var impact_spark = DynamicResources.impact_spark.instance()
			impact_spark.translation = collision_point
			get_tree().get_root().add_child(impact_spark)

func _create_bullet_impact_concrete_brick_sound():
	var boardcast_packet = {
		"weapon_hand": get_parent().controller_id,
		"impact_positions": [],
		"id": null
	}
	if ammo_consumption == 1:
		if $LaserPointer.is_colliding():
			var bullet_impact_concrete_brick_sound = DynamicResources.bullet_impact_concrete_brick_sound.instance()
			bullet_impact_concrete_brick_sound.translation = $LaserPointer.get_collision_point()
			get_tree().get_root().add_child(bullet_impact_concrete_brick_sound)
			boardcast_packet.impact_positions.append($LaserPointer.get_collision_point())
	else:
		for collision_point in $LaserPointer.get_collision_points():
			var bullet_impact_concrete_brick_sound = DynamicResources.bullet_impact_concrete_brick_sound.instance()
			bullet_impact_concrete_brick_sound.translation = collision_point
			get_tree().get_root().add_child(bullet_impact_concrete_brick_sound)
			boardcast_packet.impact_positions.append(collision_point)
	NetworkPeerController.boardcast_fire_packet(boardcast_packet)

func _process_hit():
	if ammo_consumption == 1:
		if $LaserPointer.is_colliding():
			if $LaserPointer.get_collision_object().get_class() == "Area" and ($LaserPointer.get_collision_object().get_collision_layer_bit(6) or $LaserPointer.get_collision_object().get_collision_layer_bit(7)):
				# hit object is area and it is interactive key
				$LaserPointer.get_collision_object().hit()
			elif $LaserPointer.get_collision_object().get_class() == "Area" and $LaserPointer.get_collision_object().get_collision_layer_bit(3):
				# hit object is area and it is enemy hitbox
				$LaserPointer.get_collision_object().hit(int(_get_damage_by_random_range() * NetworkPeerController.damage_multiplier))
	else:
		for collision_object in $LaserPointer.get_collision_objects():
			if collision_object.get_class() == "Area" and collision_object.get_collision_layer_bit(3):
				collision_object.hit(int(_get_damage_by_random_range() * NetworkPeerController.damage_multiplier))

func _get_damage_by_random_range():
	randomize()
	return (damage + (randi() % ((damage_range * 2) + 1) - damage_range))
