# assign this script to mesh node of weapon

extends MeshInstance

var accent_material

func _ready():
	accent_material = SpatialMaterial.new()
	_set_accent_color(Helper.available_colors[SettingsWeapon.accent_color])
	
func _set_accent_color(color):
	accent_material.albedo_color = color
	accent_material.metallic = 0.75
	accent_material.roughness = 0.5
	set_surface_material(1, accent_material)

func update_color():
	_set_accent_color(Helper.available_colors[SettingsWeapon.accent_color])
