extends Spatial

const MUZZLE_FIRE_LIGHT_TIMEOUT = 0.04

var muzzle_fire_light_timer
var this_enemy_instance

var shot_sound_count

func _ready():
	# var init
	muzzle_fire_light_timer = 0
	this_enemy_instance = get_parent().get_parent()

	shot_sound_count = $ShotSounds.get_child_count()
	# var init end

	var accent_material = SpatialMaterial.new()
	
	accent_material.albedo_color = Helper.available_colors[this_enemy_instance.weapon_accent_color]
	accent_material.metallic = 0.75
	accent_material.roughness = 0.5
	
	get_child(0).set_surface_material(1, accent_material)

func _process(delta):
	if $MuzzleFireLight.visible:
		muzzle_fire_light_timer += delta
		if muzzle_fire_light_timer >= MUZZLE_FIRE_LIGHT_TIMEOUT:
			$MuzzleFireLight.visible = false
			muzzle_fire_light_timer = 0

func fire():
	# play random bullet leave barrel sound
	$BulletLeaveBarrelSound.play_random()
		
	# play random shot sound
	randomize()
	while $ShotSounds.get_child(randi() % shot_sound_count).playing:
		randomize()
	$ShotSounds.get_child(randi() % shot_sound_count).play()

	# flash muzzle fire light
	$MuzzleFireLight.visible = true

	# display discharge fire spark
	$DischargeSpark.discharge()

	print("enemy weapon fired: " + get_parent().name)
