extends AudioStreamPlayer3D

var bullet_impact_concrete_brick_samples
var sample_count

func _ready():
	# var init
	bullet_impact_concrete_brick_samples = []
	
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_01.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_02.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_03.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_04.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_05.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_06.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_07.wav"))
	bullet_impact_concrete_brick_samples.append(load("res://assets/weapons/components/sounds/bullet_impact_concrete_brick_08.wav"))
	
	sample_count = bullet_impact_concrete_brick_samples.size()
	# var init end
	
	_play_random()
		
func _play_random():
	randomize()
	stream = bullet_impact_concrete_brick_samples[randi() % sample_count]
	play()

func _process(_delta):
	if not playing:
		queue_free()
