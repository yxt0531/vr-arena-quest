extends AudioStreamPlayer3D

var bullet_leave_barrel_samples
var sample_count

func _ready():
	# var init
	bullet_leave_barrel_samples = []
	
	bullet_leave_barrel_samples.append(load("res://assets/weapons/components/sounds/bullet_leave_barrel_01.wav"))
	bullet_leave_barrel_samples.append(load("res://assets/weapons/components/sounds/bullet_leave_barrel_02.wav"))
	bullet_leave_barrel_samples.append(load("res://assets/weapons/components/sounds/bullet_leave_barrel_03.wav"))
	bullet_leave_barrel_samples.append(load("res://assets/weapons/components/sounds/bullet_leave_barrel_04.wav"))
	bullet_leave_barrel_samples.append(load("res://assets/weapons/components/sounds/bullet_leave_barrel_05.wav"))
	
	sample_count = bullet_leave_barrel_samples.size()
	# var init end
	
func play_random():
	randomize()
	stream = bullet_leave_barrel_samples[randi() % sample_count]
	play()
