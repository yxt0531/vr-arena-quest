extends Spatial

const DURATION = 0.05

var discharge_timer

func _ready():
	# var init
	discharge_timer = 0
	# var init end

func _process(delta):
	if visible:
		discharge_timer += delta
		if discharge_timer >= DURATION:
			visible = false
			discharge_timer = 0

func discharge():
	rotate_z(randi() % 360)
	visible = true
	discharge_timer = 0
