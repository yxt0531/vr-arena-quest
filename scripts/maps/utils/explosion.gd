extends CPUParticles

const EXPLOSION_FLASH_TIMEOUT = 0.04

var flash_timer

func _ready():
	flash_timer = 0
	
	emitting = true
	$ExplosionSoundPlayer.play()

func _process(delta):
	flash_timer += delta
	if flash_timer >= EXPLOSION_FLASH_TIMEOUT:
		$Flash.visible = false
	
	if not emitting and not $ExplosionSoundPlayer.playing:
		queue_free()
