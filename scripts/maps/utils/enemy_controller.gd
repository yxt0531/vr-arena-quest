extends Spatial

var spawned_enemies

func _ready():
	# var init
	spawned_enemies = []
	# var init end
	
	NetworkPeerController.enemy_controller = self

func _exit_tree():
	NetworkPeerController.enemy_controller = null

func _process(delta):
	if NetworkPeerController.enemy_controller == null:
		NetworkPeerController.enemy_controller = self
	
	if NetworkPeerController.enemies.size() > 0:
		for enemy in NetworkPeerController.enemies.keys():
			if not spawned_enemies.has(enemy):
				# new enemy
				_spawn_enemy(enemy)
				spawned_enemies.append(enemy) # append new enemy to spawned enemies array
				
	_process_enemy()
	
func _process_enemy():
	for enemy_instances in get_children():
		# print(int(enemy_instances.name))
		if not NetworkPeerController.enemies.has(int(enemy_instances.name)):
			enemy_instances.queue_free()
			# remove enemy if enemy player disconnected
		else:
			# update enemy stats here

			if (NetworkPeerController.enemies[int(enemy_instances.name)].current_map) == "MainMenu":
				enemy_instances.visible = false
			else:
				enemy_instances.visible = true
				
			enemy_instances.translation = NetworkPeerController.enemies[int(enemy_instances.name)].translation
			enemy_instances.rotation_degrees = NetworkPeerController.enemies[int(enemy_instances.name)].rotation_degrees
			
			enemy_instances.get_node("Hitbox").translation = NetworkPeerController.enemies[int(enemy_instances.name)].hitbox_translation
			enemy_instances.get_node("Hitbox").rotation_degrees = NetworkPeerController.enemies[int(enemy_instances.name)].hitbox_rotation_degrees

			enemy_instances.get_node("LeftWeaponPos").translation = NetworkPeerController.enemies[int(enemy_instances.name)].left_weapon_translation
			enemy_instances.get_node("LeftWeaponPos").rotation_degrees = NetworkPeerController.enemies[int(enemy_instances.name)].left_weapon_rotation_degrees
			enemy_instances.get_node("RightWeaponPos").translation = NetworkPeerController.enemies[int(enemy_instances.name)].right_weapon_translation
			enemy_instances.get_node("RightWeaponPos").rotation_degrees = NetworkPeerController.enemies[int(enemy_instances.name)].right_weapon_rotation_degrees

			enemy_instances.left_weapon = NetworkPeerController.enemies[int(enemy_instances.name)].left_weapon
			enemy_instances.left_weapon_laser_on = NetworkPeerController.enemies[int(enemy_instances.name)].left_weapon_laser_on

			enemy_instances.right_weapon = NetworkPeerController.enemies[int(enemy_instances.name)].right_weapon
			enemy_instances.right_weapon_laser_on = NetworkPeerController.enemies[int(enemy_instances.name)].right_weapon_laser_on

func _spawn_enemy(enemy_id):
	if NetworkPeerController.enemies.has(enemy_id):
		var new_enemy = DynamicResources.enemy.instance()
		new_enemy.name = str(enemy_id)

		new_enemy.nickname = NetworkPeerController.enemies[enemy_id].nickname

		new_enemy.weapon_accent_color = NetworkPeerController.enemies[enemy_id].weapon_accent_color
		new_enemy.weapon_laser_color = NetworkPeerController.enemies[enemy_id].weapon_laser_color

		new_enemy.avatar_primary_color = NetworkPeerController.enemies[enemy_id].avatar_primary_color
		new_enemy.avatar_secondary_color = NetworkPeerController.enemies[enemy_id].avatar_secondary_color
		new_enemy.avatar_accent_color = NetworkPeerController.enemies[enemy_id].avatar_accent_color
		new_enemy.avatar_emissive_color = NetworkPeerController.enemies[enemy_id].avatar_emissive_color

		new_enemy.update_color()

		add_child(new_enemy)
		print("new enemy spawned: " + str(enemy_id))

func generate_enemy_fire_objects(packet):
	for impact_position in packet.impact_positions:
		var impact_spark = DynamicResources.impact_spark.instance()
		impact_spark.translation = impact_position
		get_tree().get_root().add_child(impact_spark)
		
		var bullet_impact_concrete_brick_sound = DynamicResources.bullet_impact_concrete_brick_sound.instance()
		bullet_impact_concrete_brick_sound.translation = impact_position
		get_tree().get_root().add_child(bullet_impact_concrete_brick_sound)
	
	for enemy_instance in get_children():
		if enemy_instance.name == str(packet.id):
			match packet.weapon_hand:
				1:
					if enemy_instance.get_node("LeftWeaponPos").get_child(1) != null:
						enemy_instance.get_node("LeftWeaponPos").get_child(1).fire()
				2:
					if enemy_instance.get_node("RightWeaponPos").get_child(1) != null:
						enemy_instance.get_node("RightWeaponPos").get_child(1).fire()
						
	print("fire objects generated")

func hit_received(dmg):
	if get_parent().find_node("Player") != null:
		get_parent().find_node("Player").current_health -= dmg

func kill_received(nickname, death_position):
	if get_parent().find_node("Player") != null:
		var explosion = DynamicResources.explosion.instance()
		explosion.translation = death_position
		get_tree().get_root().add_child(explosion)
		get_parent().get_node("Player/UpperHUDViewport/UpperHUDUI/LabelInfo").text = nickname + " Eliminated"

func wps_deactivation_received(wps_name):
	if get_parent().find_node(wps_name) != null:
		get_parent().find_node(wps_name).deactivate(true)
