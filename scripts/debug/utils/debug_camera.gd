extends Camera

const CAMERA_TURN_SPEED = 200

var prev_active_cam
var yaw

func _ready():
	# var init
	prev_active_cam = null
	yaw = get_parent()
	# var init end
	
func _process(_delta):
	if Input.is_action_just_pressed("debug_toggle_debug_camera"):
		if not current:
			prev_active_cam = get_viewport().get_camera()
			prev_active_cam.current = false
			current = true
			
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		else:
			current = false
			prev_active_cam.current = true
			
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _input(event):
	if current:
		if event is InputEventMouseMotion:
			return mouse(event)
			
func look_updown_rotation(rotation = 0):
	var toReturn = self.get_rotation() + Vector3(rotation, 0, 0)
	toReturn.x = clamp(toReturn.x, PI / -2, PI / 2)

	return toReturn

func look_leftright_rotation(rotation = 0):
	return yaw.get_rotation() + Vector3(0, rotation, 0)

func mouse(event):
	yaw.set_rotation(look_leftright_rotation(event.relative.x / -CAMERA_TURN_SPEED))
	self.set_rotation(look_updown_rotation(event.relative.y / -CAMERA_TURN_SPEED))
