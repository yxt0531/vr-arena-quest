extends Control

func _ready():
	if get_parent().get_node("Player/ARVROrigin").arvr_interface:
		queue_free()

func _on_ButtonJoin_button_down():
	get_parent().get_node("3DUISwitch/Screen0/ScreenMain/3DTextbox").set_text($InputAddress.text)
	get_parent().get_node("3DUISwitch/Screen0/ScreenMain/ButtonJoin").hit()

func _on_ButtonJoinReturn_button_down():
	get_parent().get_node("3DUISwitch/Screen1/ScreenJoin/ButtonReturn").hit()
